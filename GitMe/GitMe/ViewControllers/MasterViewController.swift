//
//  MasterViewController.swift
//  GitMe
//
//  Created by Matyas Varga on 2019. 11. 25..
//  Copyright © 2019. Matyas Varga. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var repoList = [Repository]()
    let dataLoader = DataCollector()


    override func viewDidLoad() {
        super.viewDidLoad()
        dataLoader.loadRepositories(for: self)
        // Do any additional setup after loading the view.

        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                //let repository = repoList[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                let repoToPass = repoList[indexPath.row]
                controller.title = repoToPass.name
                controller.repository = repoToPass
                controller.dataCollector = self.dataLoader
                //assign data to detailviewcontroller
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
                detailViewController = controller
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.repoList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReusableInfoCell", for: indexPath)
        let repository = repoList[indexPath.row]
        guard let reusableCell = cell as? ReusableInfoCell else {
            return cell
        }
        reusableCell.repositoryName.text = repository.name
        reusableCell.repositoryDescription.text = repository.fullName
        return reusableCell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52.0
    }
}

