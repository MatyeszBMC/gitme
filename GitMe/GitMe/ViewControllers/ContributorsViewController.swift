//
//  ContributorsViewController.swift
//  GitMe
//
//  Created by Matyas Varga on 2019. 11. 30..
//  Copyright © 2019. Matyas Varga. All rights reserved.
//

import UIKit

class ContributorsViewController: UITableViewController {
    
    var contributorList = [Contributor]()
    var dataCollector: DataCollector?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contributorList.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContributorCell", for: indexPath)
        let contributor = contributorList[indexPath.row]
        guard let reusableCell = cell as? ContributorCell else {
            return cell
        }
        reusableCell.userName.text = contributor.loginName
        reusableCell.userImage.image = self.dataCollector?.loadDefaultImage()
        
        if let imageURL = URL(string: contributor.avatarURL) {
            self.dataCollector?.loadImage(into: reusableCell, from: imageURL)
        }
        
        return reusableCell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return false
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52.0
    }
    
}
