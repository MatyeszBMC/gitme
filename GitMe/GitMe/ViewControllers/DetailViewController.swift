//
//  DetailViewController.swift
//  GitMe
//
//  Created by Matyas Varga on 2019. 11. 25..
//  Copyright © 2019. Matyas Varga. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    var repository: Repository?
    var dataCollector: DataCollector?
    
    @IBOutlet weak var sizeDetailLabel: UILabel!
    @IBOutlet weak var stargazersDetailLabel: UILabel!
    @IBOutlet weak var forksDetailLabel: UILabel!
    
    func configureView() {
        // Update the user interface for the detail item.
        if let repositoryNotNil = repository {
            sizeDetailLabel.text = String(repositoryNotNil.size)
            stargazersDetailLabel.text = String(repositoryNotNil.stargazers)
            forksDetailLabel.text = String(repositoryNotNil.forks)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showContributors",
            let navigationController = segue.destination as? UINavigationController,
            let contributorVC = navigationController.viewControllers.first as? ContributorsViewController
        {
            contributorVC.dataCollector = self.dataCollector
            if let repositoryNotNil = repository {
                contributorVC.dataCollector?.loadContributors(for: repositoryNotNil, in: contributorVC)
            } else {
                //TODO: insert message about the empty repository
            }
        }
    }
}

