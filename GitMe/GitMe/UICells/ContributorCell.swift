//
//  ContributorCell.swift
//  GitMe
//
//  Created by Matyas Varga on 2019. 11. 30..
//  Copyright © 2019. Matyas Varga. All rights reserved.
//

import UIKit

class ContributorCell: UITableViewCell {
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
