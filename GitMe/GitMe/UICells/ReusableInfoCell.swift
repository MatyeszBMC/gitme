//
//  ReusableInfoCell.swift
//  GitMe
//
//  Created by Matyas Varga on 2019. 11. 28..
//  Copyright © 2019. Matyas Varga. All rights reserved.
//

import UIKit

class ReusableInfoCell: UITableViewCell {
    
    @IBOutlet weak var repositoryName: UILabel!
    @IBOutlet weak var repositoryDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
