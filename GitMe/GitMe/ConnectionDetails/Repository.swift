//
//  Repository.swift
//  GitMe
//
//  Created by Matyas Varga on 2019. 11. 26..
//  Copyright © 2019. Matyas Varga. All rights reserved.
//

import Foundation

struct Repository {
    let name: String
    let fullName: String
    let size: Int
    let stargazers: Int
    let forks: Int
    let contributorsURL: String
    
    public init?(from data: Any) {
        if let dataNotNil = data as? [String: Any],
            let name = dataNotNil["name"] as? String,
            let fullName = dataNotNil["full_name"] as? String,
            let size = dataNotNil["size"] as? Int,
            let stargazers = dataNotNil["stargazers_count"] as? Int,
            let forks = dataNotNil["forks_count"] as? Int,
            let contributorsURL = dataNotNil["contributors_url"] as? String
        {
            self.name = name
            self.fullName = fullName
            self.size = size
            self.stargazers = stargazers
            self.forks = forks
            self.contributorsURL = contributorsURL
        } else {
            return nil
        }
    }
}
