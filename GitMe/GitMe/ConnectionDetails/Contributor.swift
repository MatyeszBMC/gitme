//
//  Contributor.swift
//  GitMe
//
//  Created by Matyas Varga on 2019. 11. 26..
//  Copyright © 2019. Matyas Varga. All rights reserved.
//

import Foundation

struct Contributor {
    let loginName: String
    let avatarURL: String
    
    public init?(from data: Any){
        if let dataNotNil = data as? [String: Any],
            let loginName = dataNotNil["login"] as? String,
            let avatarURLString = dataNotNil["avatar_url"] as? String
        {
            self.loginName = loginName
            self.avatarURL = avatarURLString
        } else {
            return nil
        }
    }
}
