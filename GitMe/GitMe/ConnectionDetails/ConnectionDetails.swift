//
//  ConnectionDetails.swift
//  GitMe
//
//  Created by Matyas Varga on 2019. 11. 26..
//  Copyright © 2019. Matyas Varga. All rights reserved.
//

import Foundation

enum ConnectionDetails {
    public static let githubBaseUrl = "https://api.github.com/"
    public static let search = "search/"
    public static let repositories = "repositories"
    public static let query = "?q="
    public static let swiftLanguage = "+language:swift"
    public static let accessToken = "&accesstoken=df6f765c265c02c1ef978f6ee3207407cf878f4d"
    public static let repositoriesURL = URL(string: ConnectionDetails.githubBaseUrl + ConnectionDetails.search + ConnectionDetails.repositories + ConnectionDetails.query + ConnectionDetails.swiftLanguage)
    //"https://api.github.com/search/repositories?q=+language:swift&sort=stars&order=desc&accesstoken=df6f765c265c02c1ef978f6ee3207407cf878f4d"
    public static let defaultAvatar = "default-avatar.jpg"
}


