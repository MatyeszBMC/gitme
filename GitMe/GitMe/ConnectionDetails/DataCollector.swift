//
//  MasterViewControllerExtension.swift
//  GitMe
//
//  Created by Matyas Varga on 2019. 11. 26..
//  Copyright © 2019. Matyas Varga. All rights reserved.
//

import UIKit
import Foundation

class DataCollector {
    public func loadRepositories(for viewController: UIViewController) {
        DispatchQueue.global().async {
            guard let url = ConnectionDetails.repositoriesURL else {
                print("The URL is not valid.")
                return
            }
            let request = URLRequest(url: url, cachePolicy: .returnCacheDataElseLoad)
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                if let error = error {
                    print("Error happened during json download: \(error)")
                    return
                }
                if let response = response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let data = data,
                    let jsonData = try? JSONSerialization.jsonObject(with: data, options: .allowFragments),
                    let jsonDictionary = jsonData as? [String: Any]
                {
                    guard let repositories = jsonDictionary["items"] as? [Any], let masterVC = viewController as? MasterViewController else {
                        print("The jsonDictionary is not complete.")
                        return
                    }
                    
                    for repoData in repositories {
                        if let repository = Repository(from: repoData) {
                            masterVC.repoList.append(repository)
                        } else {
                            continue
                        }
                    }
                    DispatchQueue.main.async {
                        masterVC.tableView.reloadData()
                    }
                }
            }
            task.resume()
        }
    }
    
    //Load contributors only when they are needed for UI
    public func loadContributors(for repository: Repository, in viewController: UIViewController) {
        DispatchQueue.global().async {
            guard let url = URL(string: repository.contributorsURL), let contributorVC = viewController as? ContributorsViewController else {
                print("The URL is not valid.")
                return
            }
            let request = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData)
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                if let error = error {
                    print("Error happened during json download: \(error)")
                    return
                }
                if let response = response as? HTTPURLResponse,
                    response.statusCode == 200,
                    let data = data,
                    let jsonData = try? JSONSerialization.jsonObject(with: data, options: .allowFragments),
                    let jsonRawList = jsonData as? [Any]
                {
                    for contributorRawData in jsonRawList {
                        if let contributor = Contributor(from: contributorRawData) {
                            contributorVC.contributorList.append(contributor)
                        } else {
                            print("There was an issue with contributor raw data")
                            continue
                        }
                    }
                    DispatchQueue.main.async {
                        //viewController.contributors = contributorList
                        //update list of contributors on UI
                        contributorVC.tableView.reloadData()
                    }
                }
            }
            task.resume()
        }
    }
    
    func loadImage(into cell: ContributorCell, from url: URL) {
        var image: UIImage?
        
        DispatchQueue.global().async {
            do {
                try image = UIImage(data: Data(contentsOf: url))!
            } catch {
                print("Image can not be loaded.")
            }
            DispatchQueue.main.async(execute: {
                cell.activityIndicator.stopAnimating()
                cell.activityIndicator.isHidden = true
                if image != nil {
                    cell.userImage.image = image
                }
            })
        }
    }
    
    func loadDefaultImage() -> UIImage? {
        return UIImage(named: ConnectionDetails.defaultAvatar, in: Bundle.main, compatibleWith: nil)
    }
}
